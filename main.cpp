#include <libpq-fe.h>
#include <iostream>
#include <string>
#include <iomanip>
#include <string.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>

enum CODES_RETOURS
{
  TOUT_VA_BIEN,
  ERR_PING_REJECT,
  ERR_PING_NORESPONSE,
  ERR_PING_NOATTEMPT,
  ERR_PING_UNKNOWN,
};


int main()
{
  unsigned int code_retour = TOUT_VA_BIEN;
  std::string conninfo = "host=postgresql.bts-malraux72.net port=5432";
  unsigned short int nb_caracteres;
  PGPing ping_result;
  ping_result = PQping(conninfo.c_str());

  if(ping_result == PQPING_OK)
  {
    PGconn *dbconn;
    PGresult *resultat_requete;
    char *reponse_requete;
    ExecStatusType etat_resultat;
    std::string db = " dbname=a.foulon";
    std::string uname = " user=a.foulon";
    std::string pass = " password=P@ssword";
    std::cout << "Le serveur est en cours d'exécution et semble accepter les connexions." << std::endl;
    conninfo += db;
    conninfo += uname;
    conninfo += pass;
    dbconn = PQconnectdb(conninfo.c_str());

    if(PQstatus(dbconn) == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de données '" << PQhost(dbconn)  << "' a été établie avec les paramètres suivants :" << std::endl;
      std::cout << " * utilisateur : " << PQuser(dbconn) << std::endl;
      std::cout << " * mot de passe : ";
      nb_caracteres = sizeof PQpass(dbconn);

      for(unsigned int i = 0; i < nb_caracteres; ++i)
      {
        std::cout << '*';
      }

      std::cout << " la longueur du mot de passe" << std::endl;
      std::cout << " * base de données : " << PQdb(dbconn) << std::endl;
      std::cout << " * port TCP : " << PQport(dbconn) << std::endl;
      std::cout << std::boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(dbconn) != 0) << std::endl; //boolalpha Ecrit/lit les données de type bool sous forme textuelle, càd true ou false.
      std::cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(dbconn)) << std::endl;
      std::cout << " * version du protocole : " << PQprotocolVersion(dbconn) << std::endl;
      std::cout << " * version du serveur : " << PQserverVersion(dbconn) << std::endl;
      std::cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
      resultat_requete = PQexec(dbconn, "SET SCHEMA 'si6'; SELECT \"Animal\".id,\"Animal\".nom,sexe,date_naissance AS \"date de naissance\" ,commentaires,\"Race\".nom as race, description FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");
      etat_resultat = PQresultStatus(resultat_requete);
      int nbLignes;
      int nbColonnes;
      int l, c;
      char *nom_colonne;
      nbLignes = PQntuples(resultat_requete);
      nbColonnes = PQnfields(resultat_requete);
      std::cout << "nombre de lignes: " << nbLignes << std::endl;
      std::cout << "nombre de colonnes: " << nbColonnes << std::endl;
      std::cout << "réponse : " <<  std::endl;
      int longueurTitre = 0;

      for(c = 0; c < nbColonnes; c++)
      {
        nom_colonne = PQfname(resultat_requete, c);

        if(longueurTitre < strlen(nom_colonne))
        {
          longueurTitre = strlen(nom_colonne);
        }
      }

      for(int compteur = 0; compteur <= (1 + nbColonnes)*longueurTitre; compteur++)
      {
        std::cout << "-";
      }

      std::cout << "\n";

      for(c = 0; c < nbColonnes; c++)
      {
        nom_colonne = PQfname(resultat_requete, c);
        std::cout << std::left << std::setw(longueurTitre) << nom_colonne << " | " << std::flush;
      }

      std::cout << "\n";

      for(int compteur = 0; compteur <= longueurTitre * (nbColonnes + 1); compteur++)
      {
        std::cout << "-";
      }

      std::cout << "\n";

      for(l = 0; l < nbLignes; l++)
      {
        for(c = 0; c < nbColonnes; c++)
        {
          reponse_requete = PQgetvalue(resultat_requete, l, c);

          if(strlen(reponse_requete) <= longueurTitre)
          {
            std::cout << std::left << std::setw(longueurTitre) << reponse_requete << " | " << std::flush;
          }
          else
          {
            //reponse_requete.subString(0, longueurTitre-3);
            //substr(reponse_requete, 0, longueurTitre-3);
            // std::cout << substr (reponse_requete,0,(longueurTitre-3))  << std::flush;
            char *chaine = reponse_requete;
            chaine[longueurTitre] = '\0';
            chaine[longueurTitre - 1] = '.';
            chaine[longueurTitre - 2] = '.';
            chaine[longueurTitre - 3] = '.';
            std::cout << std::left << std::setw(longueurTitre) << chaine << " | " << std::flush;
          }
        }

        std::cout << "\n";

        for(int compteur = 0; compteur <= longueurTitre * (nbColonnes + 1); compteur++)
        {
          std::cout << "-";
        }

        std::cout << "\n" << std::endl;
      }
    }
  }
  else if(ping_result == PQPING_REJECT)
  {
    std::cerr << "Le serveur est en cours d'exécution mais est dans un état qui interdit les connexions (démarrage, arrêt, restauration après crash)." << std::endl;
    code_retour = ERR_PING_REJECT;
  }
  else if(ping_result == PQPING_NO_RESPONSE)
  {
    std::cerr << "Le serveur n'a pas pu être contacté. Cela pourrait indiquer que le serveur n'est pas en cours d'exécution ou qu'il y a un problème avec les paramètres de connexion donnés (par exemple un mauvais numéro de port). Cela peut aussi indiquer un problème de connexion réseau (par exemple un pare-feu qui bloque la demande de connexion)." << std::endl;
    code_retour = ERR_PING_NORESPONSE;
  }
  else if(ping_result == PQPING_NO_ATTEMPT)
  {
    std::cerr << "Aucune tentative n'a été faite pour contacter le serveur à cause des paramètres fournis erronnés ou à cause d'un problème au niveau client (par exemple un manque mémoire)." << std::endl;
    code_retour = ERR_PING_NOATTEMPT;
  }
  else
  {
    std::cerr << "Erreur de ping inconnue" << std::endl;
    code_retour = ERR_PING_UNKNOWN;
  }

  return code_retour;
}

